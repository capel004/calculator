﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        int plus_minus = 0;
        string number_1 = "0";
        public MainPage()
        {
            InitializeComponent();
        }

        private void Clear(object sender, EventArgs e) => calculator_value.Text = "0";

        private void Clicked_7(object sender, EventArgs e) 
        {
            if (calculator_value.Text=="0")
            {
                calculator_value.Text = "7";
            }
            else
            calculator_value.Text = calculator_value.Text + "7";
        }

        private void Clicked_8(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                calculator_value.Text = "8";
            }
            else
            calculator_value.Text = calculator_value.Text + "8";
        }

        private void Clicked_9(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                calculator_value.Text = "9";
            }
            else
                calculator_value.Text = calculator_value.Text + "9";
        }

        private void Clicked_4(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                calculator_value.Text = "4";
            }
            else
                calculator_value.Text = calculator_value.Text + "4";
        }

        private void Clicked_5(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                calculator_value.Text = "5";
            }
            else
                calculator_value.Text = calculator_value.Text + "5";
        }

        private void Clicked_6(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                calculator_value.Text = "6";
            }
            else
                calculator_value.Text = calculator_value.Text + "6";
        }

        private void Clicked_1(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                calculator_value.Text = "1";
            }
            else
                calculator_value.Text = calculator_value.Text + "1";
        }

        private void Clicked_2(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                calculator_value.Text = "2";
            }
            else
                calculator_value.Text = calculator_value.Text + "2";
        }

        private void Clicked_3(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                calculator_value.Text = "3";
            }
            else
                calculator_value.Text = calculator_value.Text + "3";
        }

        private void Clicked_0(object sender, EventArgs e)
        {
            if (calculator_value.Text == "0")
            {
                
            }
            else
                calculator_value.Text = calculator_value.Text + "0";
        }

        private void Button_Clicked_plus (object sender, EventArgs e)
        {
            plus_minus = 1;
            number_1 = calculator_value.Text;
            calculator_value.Text = "0";
        }

        private void Button_Clicked_minus(object sender, EventArgs e)
        {
            plus_minus = -1;
            number_1 = calculator_value.Text ;
            calculator_value.Text = "0";
            
        }
        
        private void Button_Clicked_equal(object sender, EventArgs e)
        {
            if(plus_minus== 1)
            {
                int num1 = Int32.Parse(number_1);
                int num2 = Int32.Parse(calculator_value.Text);
num2 = num1 + num2;
                calculator_value.Text = num2.ToString();
            }   

            else if(plus_minus== -1)
            {
                int num1 = Int32.Parse(number_1);
                int num2 = Int32.Parse(calculator_value.Text);
                num2 = num1 - num2;
                calculator_value.Text = num2.ToString();
            }
            else
            {

            }
            plus_minus = 0;
        }
    }
    
}
